# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/10/1 21:25

"""
创建测试用例模板，检测用例模板合法性
"""

from openpyxl.styles import Font, PatternFill
from openpyxl.utils import get_column_letter

import openpyxl



# 测试用例excel表标题字段
CaseTitle = ["CaseId", "CaseName", "CaseDescription", "IsRun", "EndTime", "Result"]
StepTitle = ["StepId", "StepDescription", "KeyWord", "By", "Path", "Value", "TestTime", "Result", "ErrorInfo",
             "ErrorPicture"]


def create_case_temp():
    """
    生成测试用例模板
    :return:
    """
    wb = openpyxl.Workbook()
    ws = wb.active
    ws.title = "测试用例"
    wb.create_sheet(title="这里的表名对应测试用例CaseId编号", index=1)
    # 设置第一行单元格高宽
    ws.row_dimensions[1].height = 20
    for i in range(1, len(CaseTitle) + 1):
        ws.column_dimensions[get_column_letter(i)].width = 30
        # 设置用例表字段
        ws.cell(1, i).value = CaseTitle[i - 1]
        # 设置字段样式
        ws.cell(1, i).font = Font(bold=True, size=16)
        # 设置底色
        ws.cell(1, i).fill = PatternFill("solid", fgColor="4498e9")

    # 插入一条备注数据
    data = ["test_login_001", "xxxxxx", "xxxxx", "这里只能填写“y”或者“n”"]
    for i in range(1, len(data) + 1):
        ws.cell(2, i).value = data[i - 1]

    # 设置第一个用例步骤表模板
    ws_step = wb[wb.sheetnames[1]]
    # 设置第一行单元格高宽
    ws_step.row_dimensions[1].height = 20
    for i in range(1, len(StepTitle) + 1):
        ws_step.column_dimensions[get_column_letter(i)].width = 25
        # 设置用例表字段
        ws_step.cell(1, i).value = StepTitle[i - 1]
        # 设置字段样式
        ws_step.cell(1, i).font = Font(bold=True, size=16)
        # 设置底色
        ws_step.cell(1, i).fill = PatternFill("solid", fgColor="4498e9")

    # 插入一条备注数据
    data_step = ["login_step_01", "xxxxxx", "关键字", "定位方式：id,name,xpath等", "元素路径", "索要输入的值，多个值用“|”分开"]
    for i in range(1, len(data_step) + 1):
        ws_step.cell(2, i).value = data_step[i - 1]

    wb.save("../caseTemp.xlsx")