# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/30 22:51

"""
测试数据统计
"""

from config.filePathConfig import statisticResultPath
from tools.operateJson import *


def result_statistics(passCount: int = None, failCount: int = None, notRun: int = None, passInfo: dict = None,
                      errorInfo: dict = None):
    """
    测试结果数据统计，用于写入测试excel报告中
    :param passCount: 通过数量
    :param failCount: 失败数量
    :param notRun: 没有运行数量
    :param errorInfo: 错误异常信息
    :param passInfo: 通过的运行日志
    :return:
    """
    data = read_json_data(statisticResultPath)
    data["pass"]["count"] += passCount if passCount is not None else 0
    if passInfo is not None:
        data["pass"]["log"] = {**passInfo, **data["pass"]["log"]}
    data["fail"]["count"] += failCount if failCount is not None else 0
    if errorInfo is not None:
        data["fail"]["errorLog"] = {**errorInfo, **data["fail"]["errorLog"]}
    data["notRun"] += notRun if notRun is not None else 0
    write_json_data(statisticResultPath, data)


def result_init():
    """
    初始化清除json文件中的测试结果数据
    :return:
    """
    resultInit = {"pass": {"count": 0, "log": {}}, "fail": {"count": 0, "errorLog": {}}, "notRun": 0}
    write_json_data(statisticResultPath, resultInit)
