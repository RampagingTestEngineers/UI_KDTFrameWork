# -*- coding: utf-8 -*-

# @Project : DDT_FrameWork
# @Author  : Mr.Deng
# @Time    : 2021/8/22 23:12

"""
相关配置文件、数据读取和写入方法
"""

from configparser import ConfigParser
from tools.logConfig import Logger

Log = Logger().origin_logger


class OperateConfig:

    @staticmethod
    def read_config_data(dataPath: str):
        """
        读取.ini配置文件
        :return:
        """
        cf = ConfigParser()
        cf.read(filenames=dataPath, encoding="utf-8")
        return cf

    @staticmethod
    def write_config_data(dataPath, section, option, value):
        """
        写入.ini配置文件
        :param dataPath: 文件路径
        :param section: 标题
        :param option: 变量
        :param value: 值
        :return:
        """
        cf = ConfigParser()
        cf.read(filenames=dataPath, encoding="utf-8")
        cf.set(section, option, value)
        with open(file=dataPath, mode="w", encoding="utf-8") as f:
            cf.write(f)
        Log.info(f"正在配置文件中写入，section：{section}，option：{option}，value：{value}")

# if __name__ == '__main__':
#     from config.filePathConfig import driverPath
#
#     rw = OperateConfigData(driverPath).read_config_data()
#     print(rw.get("driver_path", "path"))
