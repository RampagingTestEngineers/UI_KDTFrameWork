# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/23 20:57

"""
测试数据，excel数据解析
"""

from openpyxl import load_workbook
from openpyxl.styles import Font

from tools.timeTools import TimeTools


class Variable:
    # 测试用例excel中列固定对应变量
    CaseId = 1
    CaseName = 2
    CaseDescription = 3
    IsRun = 4
    EndTime = 5
    CaseResult = 6

    # 测试步骤中列对应变量
    StepId = 1
    StepDescription = 2
    KeyWord = 3
    By = 4
    Path = 5
    Value = 6
    TestTime = 7
    StepResult = 8
    ErrorInfo = 9
    ErrorPicture = 10


class ParseExcel:

    def __init__(self, excelPath: str):
        self.excel = load_workbook(excelPath)
        self.excelPath = excelPath
        self.Pass = "Pass"
        self.Fail = "Fail"
        self.PassColor = Font(bold=True, color="00FF00")
        self.FailColor = Font(bold=True, color="FF0000")

    def get_sheet_name(self) -> list:
        """
        获取excel表名称
        :return:
        """
        return self.excel.sheetnames

    def get_real_row_count(self, sheetName: str) -> int:
        """
        获取表格实际数据总行数，根据扫描第一列id，所以id不能为空
        :param sheetName:
        :return:
        """
        sheet = self.excel[sheetName]
        rowCount = 0
        maxRow = sheet.max_row
        for row in range(1, maxRow + 1):
            if sheet.cell(row, 1).value is None:
                break
            else:
                rowCount += 1
        return rowCount

    def get_real_col_count(self, sheetName: str) -> int:
        """
        获取表格实际数据总列数，根据扫描第一行标题数据，获取总列数
        :param sheetName:
        :return:
        """
        sheet = self.excel[sheetName]
        colCount = 0
        maxCol = sheet.max_column
        for col in range(1, maxCol + 1):
            if sheet.cell(1, col).value is None:
                break
            else:
                colCount += 1
        return colCount - 4  # 不包含测试结果

    def get_case_id_row(self, sheetName: str, caseId: str) -> int:
        """
        根据caseId查行数
        :param sheetName:
        :param caseId: 用例ID编号
        :return:
        """
        caseIdList = self.get_col_data(sheetName, Variable.CaseId)
        return caseIdList.index(caseId) + 2  # 索引从 0开始 列数据没有算标题所以 +2算行数

    def get_row_data(self, sheetName: str, rowNum: int) -> list:
        """
        获取一行数据
        :param sheetName: 表名
        :param rowNum: 行数
        :return:
        """
        realMaxCol = self.get_real_col_count(sheetName)
        sheet = self.excel[sheetName]
        rowData = []
        for col in range(1, realMaxCol + 1):
            rowData.append(sheet.cell(rowNum, col).value)
        return rowData

    def get_col_data(self, sheetName: str, colNum: int) -> list:
        """
        获取一列数据
        :param sheetName: 表名
        :param colNum: 列数
        :return:
        """
        realMaxRow = self.get_real_row_count(sheetName)
        sheet = self.excel[sheetName]
        colData = []
        for row in range(2, realMaxRow + 1):  # 去掉表头从第二行取值
            colData.append(sheet.cell(row, colNum).value)
        return colData

    def get_cell_data(self, sheetName: str, rowNum: int, colNum: int):
        """
        获取单元格数据
        :param sheetName:
        :param rowNum:
        :param colNum:
        :return:
        """
        sheet = self.excel[sheetName]
        return sheet.cell(rowNum, colNum).value

    def write_case_end_time(self, sheetName: str, row: int):
        """
        写入用例执行结束时间
        :param sheetName:
        :param row:
        :return:
        """
        sheet = self.excel[sheetName]
        # 写入当前时间
        sheet.cell(row, Variable.EndTime).value = TimeTools.get_now_date()
        self.excel.save(self.excelPath)

    def write_case_result(self, sheetName: str, row: int, caseResult: str):
        """
        写入用例执行结果
        :param caseResult: 用例测试结果
        :param sheetName:
        :param row:
        :return:
        """
        sheet = self.excel[sheetName]
        # 判断测试成功还是失败
        color = self.PassColor if caseResult == self.Pass else self.FailColor
        # 写入当前时间和测试结果
        sheet.cell(row, Variable.CaseResult).value = caseResult
        sheet.cell(row, Variable.CaseResult).font = color
        self.excel.save(self.excelPath)

    def write_step_end_time(self, sheetName: str, row: int):
        """
        写入步骤执行结束时间
        :param sheetName:
        :param row:
        :return:
        """
        sheet = self.excel[sheetName]
        # 写入当前时间
        sheet.cell(row, Variable.TestTime).value = TimeTools.get_now_date()
        self.excel.save(self.excelPath)

    def write_step_result(self, sheetName: str, row: int, stepResult: str):
        """
        写入步骤执行结果
        :param stepResult: 步骤执行结果
        :param sheetName: 表明
        :param row: 行数
        :return:
        """
        sheet = self.excel[sheetName]
        color = self.PassColor if stepResult == self.Pass else self.FailColor
        sheet.cell(row, Variable.StepResult).value = stepResult
        sheet.cell(row, Variable.StepResult).font = color
        self.excel.save(self.excelPath)

    def write_step_error_info(self, sheetName: str, row: int, errorInfo: str):
        """
        写入步骤执行报错内容
        :param errorInfo: 步骤执行失败错误信息
        :param sheetName:
        :param row:
        :return:
        """
        sheet = self.excel[sheetName]
        # 写入报错信息
        sheet.cell(row, Variable.ErrorInfo).value = errorInfo
        self.excel.save(self.excelPath)

    def write_step_error_pic(self, sheetName: str, row: int, errorPic: str):
        """
        写入步骤执行报错内容
        :param errorPic: 步骤执行失败截图保存路径
        :param sheetName:
        :param row:
        :return:
        """
        sheet = self.excel[sheetName]
        # 写入错误图片路径
        sheet.cell(row, Variable.ErrorPicture).value = errorPic
        self.excel.save(self.excelPath)

    def clear_step_error_info(self, sheetName: str, row: int):
        """
        清除执行成功后清除后面的错误列信息数据
        :param sheetName:
        :param row:
        :return:
        """
        sheet = self.excel[sheetName]
        # 写入报错信息
        sheet.cell(row, Variable.ErrorInfo).value = ""
        sheet.cell(row, Variable.ErrorPicture).value = ""
        self.excel.save(self.excelPath)

    def write_step_error_all_info(self, sheetName, row, stepResult, errorInfo, errorPic):
        """执行失败的步骤写入测试结果、报错信息、图片链接"""
        self.write_step_result(sheetName, row, stepResult)
        self.write_step_error_info(sheetName, row, errorInfo)
        self.write_step_error_pic(sheetName, row, errorPic)
