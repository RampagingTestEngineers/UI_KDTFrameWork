# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/30 23:37

import json


def read_json_data(dataPath) -> dict:
    """
    读取json文件
    :param dataPath:
    :return:
    """
    with open(dataPath, "r", encoding="utf-8") as f:
        return json.load(f)


def write_json_data(dataPath, writeData):
    """
    写入json文件
    :param writeData:
    :param dataPath:
    :return:
    """
    with open(dataPath, "w", encoding="utf-8") as f:
        json.dump(writeData, f, indent=4, ensure_ascii=False)
