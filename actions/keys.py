# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/29 1:51

"""
键盘使用操作
"""

from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys as _Keys

from actions.find import Find

# 键盘按键操作对应关键字
KDB = {
    "space": _Keys.SPACE, "enter": _Keys.ENTER,
    "back_space": _Keys.BACK_SPACE, "f5": _Keys.F5,
    "ctrl": _Keys.CONTROL
}


class Keys:

    def __init__(self, driver):
        self.driver = driver
        self.find = Find(driver)

    def use_keys_by_element(self, by, path, keyName):
        """
        在页面某元素上使用键盘操作
        :param by: 定位元素方法
        :param path: 元素路径
        :param keyName: 使用键盘按键名称
        :return:
        """
        self.find.find_element_visibility(by, path).send_keys(KDB[keyName])

    def use_keys(self, keyName):
        """
        没有页面元素直接只用键盘操作
        :param keyName:
        :return:
        """
        ActionChains(self.driver).send_keys(KDB[keyName]).perform()

    def use_group_keys_by_element(self, by, path, keyName, groupKeys):
        """
        在页面某元素上使用键盘组合键操作
        :param by: 定位元素方法
        :param path: 元素路径
        :param keyName: 使用键盘按键名称
        :param groupKeys: 组合键名称
        :return:
        """
        self.find.find_element_visibility(by, path).send_keys(KDB[keyName], groupKeys)

    def use_group_keys(self, keyName, groupKeys):
        """
        没有操作元素直接使用组合键
        :param keyName:
        :param groupKeys:
        :return:
        """
        ActionChains(self.driver).key_down(KDB[keyName]).send_keys(groupKeys).perform()
