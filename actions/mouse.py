# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/29 2:02


"""
鼠标操作方法等，点击长按、长按拖拽、移动到某元素上方点击
"""

from selenium.webdriver.common.action_chains import ActionChains

from actions.find import Find


class Mouse:

    def __init__(self, driver):
        self.driver = driver
        self.find = Find(driver)

    def drag_and_drop(self, by, path, by1, path1):
        """
        长按拖拽按钮从某元素到另一个元素
        **用例步骤中，by1 和 path1 写到 value列中用 ”|“ 分开**
        :param by: 需要长安的按钮
        :param path:
        :param by1: 拖拽到的元素
        :param path1:
        """
        # 查找页面开始长按的按钮及拖动结束的元素位置
        startElement = self.find.find_element_visibility(by, path)
        endElement = self.find.find_element_visibility(by1, path1)
        ActionChains(self.driver).drag_and_drop(startElement, endElement).perform()

    def move_and_click(self, by, path):
        """
        移动到某元素上并点击
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        button = self.find.find_element_visibility(by, path)
        ActionChains(self.driver).move_to_element(button).click().perform()

    def click_hold_on(self, by, path):
        """
        点击并长按按钮
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        button = self.find.find_element_visibility(by, path)
        ActionChains(self.driver).click_and_hold(button).perform()

    def drag_and_drop_by_offset(self, by, path, x_offset: int, y_offset: int):
        """
        长按按钮并移动到规定坐标量位置
        :param by: 定位方式
        :param path: 元素路径
        :param x_offset: 移动到所在位置的 x 坐标
        :param y_offset: 移动到所在位置的 y 坐标
        :return:
        """
        button = self.find.find_element_visibility(by, path)
        ActionChains(self.driver).drag_and_drop_by_offset(source=button, xoffset=x_offset, yoffset=y_offset).perform()

    def move_on_element(self, by, path):
        """
        鼠标移动到元素上面
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        element = self.find.find_element_visibility(by, path)
        ActionChains(self.driver).move_to_element(element).perform()

    def release(self):
        """
        释放长按按钮,没有按钮的情况
        :return:
        """
        ActionChains(self.driver).release()

    def release_by_button(self, by, path):
        """
        释放长按按钮
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        element = self.find.find_element_visibility(by, path)
        ActionChains(self.driver).release(element)
