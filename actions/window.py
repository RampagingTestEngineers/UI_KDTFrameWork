# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/29 2:17

"""
浏览器页面切换，切换到当前最新打开的页面，按标题名称切换、关闭窗口
"""

from tools.logConfig import Logger

Log = Logger().origin_logger


class Window:

    def __init__(self, driver):
        self.driver = driver

    def close_current_window(self):
        """
        关闭当前窗口
        :return:
        """
        self.driver.close()

    def close_window_by_title(self, title: str):
        """
        关闭当前浏览器中指定标题的窗口
        :param title:
        :return:
        """
        handleLs = self.get_window_handles()
        for handle in handleLs:
            self.driver.switch_to.window(handle)
            if self.driver.title == title:
                self.close_current_window()
                return
        Log.error(f"所有页面中没有找到标题为：{title} 的页面！！！")

    def switch_window_by_url(self, url: str):
        """
        切换到最新打开的页面中
        :param: 切换页面的url地址
        :return:
        """
        handleLs = self.get_window_handles()
        for handle in handleLs:
            self.driver.switch_to.window(handle)
            if url in self.driver.current_url:
                return
        Log.error(f"所有页面中没有找到地址为：{url} 的页面切换窗口失败！！！")

    def switch_window_by_title(self, title: str):
        """
        切换当前浏览器中指定标题的窗口
        :param title:
        :return:
        """
        handleLs = self.get_window_handles()
        for handle in handleLs:
            self.driver.switch_to.window(handle)
            if self.driver.title == title:
                return
        Log.error(f"所有页面中没有找到标题为：{title} 的页面切换窗口失败！！！")

    def get_window_handles(self) -> list:
        """
        获取浏览器中所有页面的句柄
        :return:
        """
        handles = self.driver.window_handles
        return handles

    def get_current_window_handle(self):
        """
        获取当前页面句柄
        :return:
        """
        handle = self.driver.current_window_handle
        return handle
