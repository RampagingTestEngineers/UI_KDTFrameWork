# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/23 17:23

from .browser import Browser
from .wait import Wait
from .find import Find
from .element import Element
from .frame import Frame
from .cookie import Cookie
from .javascripts import JavaScript
from .keys import Keys
from .mouse import Mouse
from .window import Window
from .select import Select
from .assertion import Assertion


class Base(Browser, Wait, Find, Element, Frame, Cookie, JavaScript, Keys, Mouse, Window, Select, Assertion):
    def __init__(self, driver):
        super().__init__(driver)
        self.find = Find(driver)
        self.wait = Wait(driver)
        self.element = Element(driver)
        self.browser = Browser(driver)
