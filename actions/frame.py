# -*- coding: utf-8 -*-

# @Project : UI_KDTFrameWork
# @Author  : Mr.Deng
# @Time    : 2021/9/24 1:41

"""
切入、切出页面中的frame框架
"""

from actions.wait import Wait


class Frame:

    def __init__(self, driver):
        self.wait = Wait(driver)

    def switch_into_frame(self, by, path):
        """
        切换进入页面frame框架
        :param by: 定位方式
        :param path: 元素路径
        :return:
        """
        self.wait.wait_frame_to_be_available(by, path)

    def switch_out_frame(self):
        """
        切换出页面frame框架
        :return:
        """
        self.wait.driver.switch_to.default_content()
